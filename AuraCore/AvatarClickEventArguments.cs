﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Aura
{
    public class AvatarClickEventArguments: AvatarWindowStateArgument
    {
        public Point ClickAvatarPercentagePoint { get; set; }
        public Point ClickAvatarAbsolutePoint { get; set; }
        public Point ClickScreenAbsolutePoint { get; set; }
        public Point ClickWindowAbsolutePoint { get; set; }
        public string[] RegionIds { get; set; }
    }
}
