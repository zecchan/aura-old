﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Aura;
using Aura.Graphic;
using AuraCore.Persona;

namespace Widget.MeasuringTool
{
    public class MeasuringTool : IAuraWidgetPlugin, IAuraEventHandlerPlugin
    {
        public Guid Guid { get; } = Guid.Parse("d55dbc6610db4d6fb59ba6940519dad0");

        public Size AvatarSize { get; set; }

        public Point ClickPoint { get; set; }

        public string[] Regions { get; set; }

        public bool Active { get; set; }

        private MeasuringWindow LastWindow { get; set; }

        public void HandleEvent(Avatar avatar, AuraPersona persona, string eventId, object eventData, EventDispatchedEventArgument args)
        {
            if (eventId == "avatar.click" && Active)
            {
                if (eventData is AvatarClickEventArguments e)
                {
                    AvatarSize = e.AvatarSize;
                    ClickPoint = e.ClickAvatarPercentagePoint;
                    Regions = e.RegionIds;
                    Show(e.ClickScreenAbsolutePoint);
                    args.Handled = true;
                }
            }

            if (eventId == "window.keydown" && eventData is KeyEventArgs ke)
            {
                if (ke.Key == Key.M)
                {
                    Active = !Active;
                    AI.DispatchEvent("console.log", "Measuring tool is " + (Active ? "active" : "inactive"));
                }
            }
        }

        public void Hide()
        {
        }

        public void LoadState()
        {

        }

        public void SaveState()
        {

        }

        public void Show(Point point)
        {
            if (LastWindow != null)
            {
                try
                {
                    LastWindow.RequestClose(this);
                }
                catch { }
            }
            var w = new MeasuringWindow();
            w.Width = AvatarSize.Width * 0.8;
            w.Height = AvatarSize.Height * 0.8;
            w.Left = point.X - w.Width / 2;
            w.Top = point.Y - w.Height / 2;
            w.lbCoord.Text = Math.Round(ClickPoint.X, 2) + ", " + Math.Round(ClickPoint.Y, 2) + (Regions != null && Regions.Length > 0 ? "\r\n(" + string.Join(", ", Regions) + ")" : "");
            LastWindow = w;
            w.Show();
        }

        public void Shutdown()
        {
        }
    }
}
