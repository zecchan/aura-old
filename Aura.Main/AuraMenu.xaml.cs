﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Aura.Main
{
    /// <summary>
    /// Interaction logic for AuraMenu.xaml
    /// </summary>
    public partial class AuraMenu : Window
    {
        public AuraMainPlugin SourcePlugin { get; internal set; }

        public AuraMenu()
        {
            InitializeComponent();
        }

        public void Prepare()
        {
            lbLayers.Items.Clear();
            foreach (var ld in AI.Avatar.Drawables.LayerDefinitions)
            {
                var cx = new CheckBox();
                cx.Content = ld.Key;
                cx.Tag = ld.Key;
                cx.IsChecked = AI.RequestData<bool>("avatar.layervisibility:" + ld.Key).All(x => x);
                cx.Checked += Cx_Checked;
                cx.Unchecked += Cx_Unchecked;
                lbLayers.Items.Add(cx);
            }

            cbForceOutfit.Items.Clear();
            cbForceOutfit.Items.Add(new ComboBoxItem()
            {
                Content = "- none -",
                Tag = null
            });
            var tags = AI.Avatar.Drawables.Sets.Outfits.Select(x => x.Value).Where(x => x != null).SelectMany(x => x.Tags).ToList();
            tags.AddRange(AI.Avatar.Drawables.Sets.Hairs.Select(x => x.Value).Where(x => x != null).SelectMany(x => x.Tags));
            tags.AddRange(AI.Avatar.Drawables.Sets.Expressions.Select(x => x.Value).Where(x => x != null).SelectMany(x => x.Tags));
            tags = tags.Distinct().ToList();
            tags.Sort();
            ComboBoxItem sel = null;
            foreach (var tag in tags)
            {
                var cbi = new ComboBoxItem();
                cbi.Content = tag;
                cbi.Tag = tag;
                if (AI.Persona.State.TagBias.ContainsKey("forceOutfit") && tag == AI.Persona.State.TagBias["forceOutfit"].Key)
                    sel = cbi;
                cbForceOutfit.Items.Add(cbi);
            }
            if (sel != null) cbForceOutfit.SelectedItem = sel;
        }

        private void Cx_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox cx)
                AI.DispatchEvent("avatar.hidelayer", cx.Tag);
        }

        private void Cx_Checked(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox cx)
                AI.DispatchEvent("avatar.showlayer", cx.Tag);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SourcePlugin.ControlPanel = null;
        }

        private void cbForceOutfit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender is ComboBox cb)
            {
                if (cb.SelectedIndex > 0)
                {
                    if (cb.SelectedItem is ComboBoxItem cbi && cbi.Tag is string tag)
                    {
                        AI.Persona.State.TagBias["forceOutfit"] = new KeyValuePair<string, int>(tag, 999);
                        return;
                    }
                }
                if (AI.Persona.State.TagBias.ContainsKey("forceOutfit"))
                    AI.Persona.State.TagBias.Remove("forceOutfit");                
            }
        }
    }
}
